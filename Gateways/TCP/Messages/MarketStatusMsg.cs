﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class MarketStatusMsg : IJsonSerializable
    {
        private int market_status;
        private DateTime dt_made;

        internal MarketStatusMsg(int market_status, DateTime dt_made) //конструктор сообщения
        {
            this.market_status = market_status;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewMarketStatus, market_status, dt_made);
        }
    }
}
