﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace RocketCore
{
    static class Tester
    {
        internal static void place_orders()
        {
            int user1 = 1;

            Stopwatch sw = new Stopwatch();

            FuncCall call = new FuncCall();
            call.Action = () =>
            {
                Sys.core.CreateAccount(user1);
                Sys.core.DepositFunds(user1, false, 999999999999999m);
                Sys.core.DepositFunds(user1, true, 999999999999999m);

                sw.Start();
                for (int i = 1; i <= 1000000; i++)
                {
                    Order order;
                    Sys.core.PlaceLimit(user1, false, 0.01m, 1m, call.FuncCallId, (int)FCSources.WebApp, out order);
                }
                sw.Stop();

                Console.WriteLine("Orders placed in " + sw.ElapsedMilliseconds + " ms");
            };

            //ставим в очередь вызов функции
            Queues.stdf_queue.Enqueue(call);
        }

        internal static void match_orders()
        {
            int user2 = 2;

            FuncCall call = new FuncCall();
            call.Action = () =>
            {
                Sys.core.CreateAccount(user2);
                Sys.core.DepositFunds(user2, false, 9999999999999999m);
                Sys.core.DepositFunds(user2, true, 9999999999999999m);

                Order order;
                StatusCodes status = Sys.core.PlaceLimit(user2, true, 100100m, 0.5m, call.FuncCallId, (int)FCSources.WebApp, out order);

                Console.WriteLine("PlaceLimit Status = " + status.ToString());
            };

            //ставим в очередь вызов функции
            Queues.stdf_queue.Enqueue(call);
        }

    }
}
